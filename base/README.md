# Base Image for the Griffin+ Build Environment based on Ubuntu 18.04

This image is built on top of Ubuntu 18.04 and brings along common stuff that is needed to build projects.
It does not contain any toolchains. Derived images add the toolchain depending on specific build scenarios.
