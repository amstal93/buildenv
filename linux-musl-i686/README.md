# Cross-Build Environment for C/C++ Projects on Linux (Target: i686)

This image derives from the [Griffin+ Base Build Environment](https://gitlab.com/griffinplus/buildenv/tree/master/base) and
adds everything that is needed to build C/C++ projects for Linux using [musl libc](https://www.musl-libc.org/).
